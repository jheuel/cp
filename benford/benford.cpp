#include <iostream>
#include <fstream>
#include <sstream>
#include <map>
#include <cstdlib>
using namespace std;

int string2int(string a);

int main(int argc, char** argv)
{
    if (argc < 2) {
        cerr << "usage: benford number" << endl;
        exit(1);
    }

    int n = string2int(argv[1]);

    srand(0);
    ofstream myfile;
    myfile.open("rand.txt");

    map<char, int> N;
    N['1'] = 0;
    N['2'] = 0;
    N['3'] = 0;
    N['4'] = 0;
    N['5'] = 0;
    N['6'] = 0;
    N['7'] = 0;
    N['8'] = 0;
    N['9'] = 0;
    for (int i = 0; i < n; i++) {
        double z = 1.0;
        for (int j = 0; j < 100; j++) {
            z = (z * rand())/RAND_MAX;
        }
        ostringstream os;
        os << z;
        string s(os.str());
        N[s[0]] += 1;
    }
    for (map<char, int>::iterator it = N.begin(); it != N.end(); it++) {
        myfile << it->first << "\t" << it->second << endl;
    }
    myfile.close();
    return 0;
}

int string2int(string a) {
    istringstream ss(a);
    int n;
    if (!(ss >> n)) {
        cerr << "Invalid number " << a << endl;
    }
    return n;
}
