#define OUTPUT(x) #x << " = " << (x) << endl
#include <vector>
#include <functional>
#include "vektor.cpp"
#include <iostream>
using namespace std;

Matrix RK4(function<Matrix(double t, Matrix r)> F, double dt, double t, Matrix R)
{
    Matrix k_1 = dt * F(t, R);
    Matrix k_2 = dt * F(t + 0.5 * dt, R + 0.5 * k_1);
    Matrix k_3 = dt * F(t + 0.5 * dt, R + 0.5 * k_2);
    Matrix k_4 = dt * F(t + dt, R + k_3);

    Matrix Ergebnis = R + 1 / 6.0f * (k_1 + 2*k_2 + 2*k_3 + k_4); // + O(dt^5)
    return Ergebnis;
}

struct ErgebnissType
{
    vector<double> t;
    vector<Matrix> RVector;
};

ErgebnissType Newton(function<Vektor(Vektor r)> F, double Schritte, double dt, Vektor v_0, Vektor r_0)
{

    Matrix R;
    R.push_back(r_0);
    R.push_back(v_0);

    auto f = [F, dt](double T, Matrix r)
    {
        Matrix A;
        A.push_back( r[1] );
        //A.push_back( F(r[0] + T*T*0.5 * F(r[0])) );
        A.push_back( F(r[0]) );
        //A.push_back( F(r[0] + T * r[1]) );
        return A;
    };

    double t = 0;
    ErgebnissType Ergebnisse;
    for (int i = 0; i < Schritte; i++, t+=dt)
    {
        R = RK4(f, dt, t, R);
        Ergebnisse.t.push_back(t);
        Ergebnisse.RVector.push_back(R);
    }
    return Ergebnisse;
}
