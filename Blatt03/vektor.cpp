#include <vector>
#include <cmath>
using namespace std;

using Vektor = vector<double>;
using Matrix = vector<Vektor>;

Vektor operator*(double a, const Vektor& b)
{
    Vektor Ergebnis;
    for (size_t i = 0; i < b.size(); i++)
    {
        Ergebnis.push_back(a * b[i]);
    }
    return Ergebnis;
}

Vektor operator+(const Vektor& a, const Vektor& b)
{
    Vektor Ergebnis;
    for (size_t i = 0; i < a.size(); i++)
    {
        Ergebnis.push_back(a[i] + b[i]);
    }
    return Ergebnis;
}

double Betrag(const Vektor& a)
{
    double betrag = 0;
    for (auto i : a)
    {
        betrag += i * i;
    }
    return sqrt(betrag);
}

Vektor Kreuzprodukt(const Vektor a, const Vektor b)
{
    Vektor Ergebnis;
    Ergebnis.push_back(a[1]*b[2] - a[2]*b[1]);
    Ergebnis.push_back(a[2]*b[0] - a[0]*b[2]);
    Ergebnis.push_back(a[0]*b[1] - a[1]*b[0]);
    return Ergebnis;
}

Matrix operator*(double a, const Matrix& b)
{
    Matrix Ergebnis;

    for (size_t i = 0; i < b.size(); i++)
    {
        Ergebnis.push_back(a * b[i]);
    }
    return Ergebnis;
}

Matrix operator+(const Matrix& a, const Matrix& b)
{
    Matrix Ergebnis;
    for (size_t i = 0; i < a.size(); i++)
    {
        Ergebnis.push_back(a[i] + b[i]);
    }
    return Ergebnis;
}

