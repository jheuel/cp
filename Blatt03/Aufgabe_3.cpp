#include <iostream>
#include <cmath>
#include "Aufgabe_1.cpp"
#include <fstream>
#include <sstream>
using namespace std;

double alpha = 1.0;
double faktor = 1.0;

void Ausgabe(ofstream& f, Matrix R)
{
    double r = 0;
    for ( auto i : R[0] )
        {
            r += i*i;
            f << scientific << i;
            f << "\t";
        }
    double pot = - 1 / sqrt(r);
    f << scientific << pot; // pot. Energie
    f << " ";

    double v = 0;
    for ( auto i : R[1] )
        {
            v += i*i;
            f << scientific << i;
            f << "\t";
        }
    f << scientific << 0.5 * v; // kin. Energie
    f << "\t";
    f << scientific << 0.5 * v + pot; // Gesamtenergie
    f << "\t";
    Vektor L = Kreuzprodukt(R[0], R[1]);
    f << scientific << Betrag(L); // Drehimpuls
    f << "\t";
    // Lenz-Runge
    Vektor LR = Kreuzprodukt(R[1], L) + (-1)/Betrag(R[0]) * R[0];
    f << scientific << LR[0];
    f << "\t";
    f << scientific << LR[1];
    f << "\t";
    f << scientific << LR[2];
    f << "\t";
}

void Speichern(string filename, string legende, ErgebnissType Ergebnisse)
{
    ofstream output;
    output.open(filename);
    output << legende << endl;
    for (size_t i = 0; i < Ergebnisse.t.size(); i += 1)
    {
        output << scientific << Ergebnisse.t[i] << "\t";
        Ausgabe(output, Ergebnisse.RVector[i]);
        output << endl;
    }
}
int main(int argc, char *argv[])
{
    bool d = false;
    if (argc == 2)
    {
        stringstream s(argv[1]);
        s >> alpha;
        if (alpha != 0)
        {
            faktor = 4;
        } else {
            alpha = 1.0;
            d = true;
        }
    }

    //double dt = 5e-5;
    double dt = 5e-2;

    double Schritte = faktor * 8.0 * M_PI / dt ;
    //Vektor r_0 = {1, 0, 0};
    //Vektor v_0 = {-0.1, -1.0, 0};
    Vektor r_0 = {1, 0, 0};
    Vektor v_0 = {-0.4, -1.2, 0};
    function<Vektor(Vektor)> F = [](Vektor r)
    {
        double R = Betrag(r);
        double Rhochdrei = pow(R, alpha + 2);
        return - alpha/Rhochdrei * r;
    };

    ErgebnissType Ergebnisse;
    cout << "---------------------------" << endl;
    cout << OUTPUT(alpha);
    cout << OUTPUT(dt);
    Ergebnisse = Newton(F, Schritte, dt, v_0, r_0);
    Matrix last = Ergebnisse.RVector.back();
    cout << OUTPUT(last[0][0])
         << OUTPUT(r_0[0]);
    cout << "---------------------------" << endl;
    cout << endl << endl;

    if (d)
    {
        Speichern(
                "build/Aufgabe_3a1.txt",
                "# t\t x\t y\t z\t Epot\t vx\t vy\t vz\t Ekin\t Eges\t L\t LRx\t LRy\t LRz\t",
                Ergebnisse
                );
    } else {
        // Aufgabe 3 d)
        Ergebnisse = Newton(F, Schritte, dt, -1 * last[1], last[0]);

        Speichern(
                "build/Aufgabe_3a1.txt",
                "# t\t x\t y\t z\t Epot\t vx\t vy\t vz\t Ekin\t Eges\t L\t LRx\t LRy\t LRz\t",
                Ergebnisse
                );
    }

    return 0;
}

