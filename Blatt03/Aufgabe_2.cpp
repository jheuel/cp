#include <iostream>
#include <cmath>
#include "Aufgabe_1.cpp"
#include <fstream>
using namespace std;


void Ausgabe(ofstream& f, Matrix R)
{
    double r = 0;
    for ( auto i : R[0] )
        {
            r += i*i;
            f << scientific << i;
            f << "\t";
        }
    f << scientific << 0.5 * r;
    f << " ";

    double v = 0;
    for ( auto i : R[1] )
        {
            v += i*i;
            f << scientific << i;
            f << "\t";
        }
    f << scientific << 0.5 * v;
    f << " ";
    f << scientific << 0.5 * v + 0.5 * r;

}

void Speichern(string filename, string legende, ErgebnissType Ergebnisse)
{
    ofstream output;
    output.open(filename);
    output << legende << endl;
    for (size_t i = 0; i < Ergebnisse.t.size(); i += 1)
    {
        output << scientific << Ergebnisse.t[i] << "\t";
        Ausgabe(output, Ergebnisse.RVector[i]);
        output << endl;
    }
}

int main()
{
    //double dt = 5e-5;
    double dt = 5e-3;
    double ganzerKreis = 2 * M_PI;
    double n = 4;
    double Faktor = ganzerKreis * n;

    double Schritte = Faktor / dt;
    Vektor r_0 = {2.0, 0};
    Vektor v_0 = {0, 0};
    function<Vektor(Vektor)> F = [](Vektor r) { return (-1) * r; };

    ErgebnissType Ergebnisse;
    cout << "---------------------------" << endl;
    cout << OUTPUT(dt)
         << OUTPUT(n);
    Ergebnisse = Newton(F, Schritte, dt, v_0, r_0);
    Matrix last = Ergebnisse.RVector.back();
    cout << OUTPUT(last[0][0])
         << OUTPUT(r_0[0]);
    cout << "---------------------------" << endl;
    cout << endl << endl;;

    Speichern(
            "build/Aufgabe_2a1.txt",
            "# t\t x\t y\t Epot\t vx\t vy\t Ekin\t Eges",
            Ergebnisse
            );

    r_0 = {2, 2};
    v_0 = {0, 2};

    cout << "---------------------------" << endl;
    cout << OUTPUT(dt)
         << OUTPUT(n);
    Ergebnisse = Newton(F, Schritte, dt, v_0, r_0);
    last = Ergebnisse.RVector.back();
    cout << OUTPUT(last[0][0])
         << OUTPUT(r_0[0]);
    cout << "---------------------------" << endl;
    cout << endl << endl;;

    Speichern(
            "build/Aufgabe_2a2.txt",
            "# t\t x\t y\t Epot\t vx\t vy\t Ekin\t Eges",
            Ergebnisse
            );

    return 0;
}

