#ifndef __VEKTOR__
#define __VEKTOR__
#include <cmath>
#include <iostream>

using vektor = std::pair<double, double>;

double operator*(const vektor& a, const vektor& b) {
  return a.first * b.first + a.second * b.second;
}

vektor operator*(const double a, const vektor& b) {
  return std::make_pair(a * b.first, a * b.second);
}

vektor operator*(const vektor& a, const double b) { return b * a; }

vektor operator/(const vektor& a, const double b) {
  return std::make_pair(a.first / b, a.second / b);
}

vektor operator+(const vektor& a, const vektor& b) {
  return std::make_pair(a.first + b.first, a.second + b.second);
}

vektor operator-(const vektor& a, const vektor& b) {
  return std::make_pair(a.first - b.first, a.second - b.second);
}

void operator+=(vektor& a, const vektor& b) { a = a + b; }

void operator-=(vektor& a, const vektor& b) { a = a - b; }

void operator*=(vektor& a, const double& b) { a = a * b; }

void operator/=(vektor& a, const double& b) { a = a / b; }

double Betrag(const vektor& a) { return sqrt(a * a); }

vektor Vektor(double x, double y) { return std::make_pair(x, y); }
#endif
