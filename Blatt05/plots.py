#!/usr/bin/env python
# encoding: utf-8

#from __future__ import division
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from matplotlib import gridspec

build_dir = 'build/'

def b(Temperatur):
    path = build_dir + Temperatur + '_'
    sns.set_context("paper")
    sns.set_context("notebook", font_scale=1.0, rc={"lines.linewidth": 1.5})
    fig = plt.figure(figsize=(8, 10))
    gs = gridspec.GridSpec(3, 1, height_ratios=[1, 1, 2])

    t = np.genfromtxt(path + 't.txt', unpack=True)
    v_ges = np.genfromtxt(path + 'v_ges.txt', unpack=True)
    ax1 = fig.add_subplot(gs[0])
    ax1.plot(t, v_ges[0], label='x')
    ax1.plot(t, v_ges[1], label='y')
    plt.setp(ax1.get_xticklabels(), visible=False)
    ax1.legend(loc='best', ncol=3)
    ax1.set_ylabel('$v_\mathrm{ges}$')

    T = np.genfromtxt(path + 'Temp.txt', unpack=True)
    ax2 = fig.add_subplot(gs[1], sharex=ax1)
    ax2.plot(t, T, label='Temperatur')
    ax2.set_ylabel('$T$')
    ax2.legend(loc='best', ncol=3)
    plt.setp(ax2.get_xticklabels(), visible=False)

    E = np.genfromtxt(path + 'E.txt', unpack=True)
    ax3 = fig.add_subplot(gs[2], sharex=ax1)
    ax3.plot(t, E[0], label='Ekin')
    ax3.plot(t, E[1], label='Epot')
    ax3.plot(t, E[2], label='Eges')
    ax3.set_xlabel('$t$')
    ax3.set_ylabel('$E$')
    ax3.legend(loc='best', ncol=3)
    fig.savefig(path + 'plots.pdf')

def c(Temperatur):
    path = build_dir + Temperatur + '_'
    sns.set_context("paper")
    #sns.set_context("notebook", font_scale=1.0, rc={"lines.linewidth": 1.5})
    fig = plt.figure()
    #gs = gridspec.GridSpec(3, 1, height_ratios=[1, 1, 2])

    hist = np.genfromtxt(path + 'histogramm.txt', unpack=True)
    ax1 = fig.add_subplot(111)
    bins = len(hist)
    x = range(0, bins)
    ax1.plot(x, hist)
    #ax1.legend(loc='best', ncol=1)
    ax1.set_ylabel('$N$')
    ax1.set_xlabel(r'$r \cdot \mathrm{bins / cutoff}$')
    fig.savefig(path + 'histogramm.pdf')


def main():
    for t in [10, 200, 1000, 2000, 2500]:
        T = str(t)
        b(T)
        c(T)

if __name__ == '__main__':
    main()

