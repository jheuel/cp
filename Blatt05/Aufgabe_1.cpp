#define OUTPUT(x) #x << " = " << (x) << endl;
#include "vektor.cpp"
#include <vector>
#include <map>
#include <sstream>
#include <random>
#include <algorithm>
#include <fstream>
#include <functional>
#include <memory>

using std::string;
using std::vector;
using std::pair;
using std::tuple;
using std::ofstream;
using std::tie;
using std::cout;
using std::endl;
using std::pow;
using std::swap;
using std::scientific;

class Konfiguration;
class TeilchenKlasse;
using Teilchenliste = vector<TeilchenKlasse>;

class TeilchenKlasse {
 public:
  TeilchenKlasse() : L(0), cutoff(0){};
  vektor Ort;
  vektor Geschwindigkeit;
  double L;
  double cutoff;
  void Randbedingungen();
};

void TeilchenKlasse::Randbedingungen() {
  Ort.first -= L * floor(Ort.first / L);
  Ort.second -= L * floor(Ort.second / L);
}

double V(vektor r) {
  double R = 1 / (r * r);
  double R3 = R * R * R;
  return 4 * R3 * (R3 - 1);
}

vektor F_LJ(const vektor r) {
  double R2 = 1 / (r * r);
  double R6 = R2 * R2 * R2;
  return 24 * R2 * R6 * (2 * R6 - 1) * r;
}

pair<vector<vektor>, double> F(vector<TeilchenKlasse> Teilchen) {
  vector<vektor> f(Teilchen.size(), Vektor(0, 0));
  double v = 0;
  for (size_t m = 0; m < Teilchen.size(); m++) {
    for (size_t n = 0; n < m; n++) {
      vektor h = Teilchen.at(m).Ort - Teilchen.at(n).Ort;

      h.first -= Teilchen.at(m).L * floor(h.first / Teilchen.at(m).L + 0.5);
      h.second -= Teilchen.at(m).L * floor(h.second / Teilchen.at(m).L + 0.5);

      if (Betrag(h) > Teilchen.at(n).cutoff) continue;

      vektor force = F_LJ(h);
      f.at(m) += force;
      f.at(n) -= force;
      v += V(h);
    }
  }
  return make_pair(f, v);
}

class Konfiguration {
 public:
  Konfiguration(int N = 16, double L = 8, double cutoff = 4, size_t bins = 500);
  void init(double T);
  void update_histo();
  vektor Schwerpunktsgeschwindigkeit();
  double Ekin();
  size_t N;
  vector<double> histogramm;
  std::vector<TeilchenKlasse> Teilchen;
};

void Konfiguration::update_histo() {
  for (size_t m = 0; m < Teilchen.size(); m++) {
    for (size_t n = 0; n < m; n++) {
      vektor h = Teilchen.at(m).Ort - Teilchen.at(n).Ort;
      double r = Betrag(h);
      size_t bin = size_t(floor(r * histogramm.size() / Teilchen.at(m).cutoff));
      histogramm[bin] += 2;
    }
  }
}

Konfiguration::Konfiguration(int N, double L, double cutoff, size_t bins)
    : N(N) {
  for (size_t i = 0; i < bins; i++) histogramm.push_back(0);
  Teilchen.resize(N);
  for (auto &t : Teilchen) {
    t.L = L;
    t.cutoff = cutoff;
  }
}

vektor Konfiguration::Schwerpunktsgeschwindigkeit() {
  vektor v_ges = Vektor(0, 0);
  for (auto i : Teilchen) {
    v_ges += i.Geschwindigkeit;
  }
  return v_ges / N;
}

double Konfiguration::Ekin() {
  double E = 0;
  for (auto i : Teilchen) {
    E += i.Geschwindigkeit * i.Geschwindigkeit;
  }
  return 0.5 * E;
}

void Konfiguration::init(double T) {
  const int Nsqrt = sqrt(N);  // nehme mal an, dass was gerades heraus kommt
  int n = 0;
  int m = 0;

  std::default_random_engine generator;
  std::uniform_real_distribution<float> distribution(0, 2 * M_PI);
  auto norm_rnd = std::bind(distribution, generator);

  vektor v_bar = Vektor(0, 0);
  for (size_t i = 0; i < N; i++, n++) {
    if (Nsqrt == n) {
      n = 0;
      m++;
    }
    Teilchen.at(i).Ort = Teilchen.at(i).L / 8. * Vektor(1 + 2 * n, 1 + 2 * m);
    double phi = norm_rnd();
    Teilchen.at(i).Geschwindigkeit = norm_rnd() * Vektor(cos(phi), sin(phi));
    v_bar += Teilchen.at(i).Geschwindigkeit;
  }
  v_bar /= N;

  for (TeilchenKlasse &t : Teilchen) {
    t.Geschwindigkeit -= v_bar;
  }

  // mit gewünschter Temperatur skalieren
  double factor = sqrt(T / (Ekin() / N));
  for (TeilchenKlasse &t : Teilchen) {
    t.Geschwindigkeit *= factor;
  }
}

tuple<Teilchenliste, vector<vektor>, double>
Verlet_Step(const Teilchenliste x, vector<vektor> F_n, double h) {
  vector<TeilchenKlasse> y(x);
  vector<vektor> F_nplus1(x.size());

  for (size_t i = 0; i < x.size(); i++) {
    y.at(i).Ort =
        x.at(i).Ort + x.at(i).Geschwindigkeit * h + 0.5 * F_n.at(i) * h * h;
  }

  for (size_t i = 0; i < x.size(); i++) {
    y.at(i).Randbedingungen();
  }
  double Epot;
  tie(F_nplus1, Epot) = F(y);

  for (size_t i = 0; i < x.size(); i++) {
    y.at(i).Geschwindigkeit =
        x.at(i).Geschwindigkeit + 0.5 * (F_nplus1.at(i) + F_n.at(i)) * h;
  }
  return std::make_tuple(y, F_nplus1, Epot);
}

void Verlet(Konfiguration System, double T_0, double h, double t_eq,
            int MAX_STEPS) {
  int eq = t_eq / h;
  int max_steps = eq + MAX_STEPS;
  string Tzusatz = std::to_string(static_cast<int>((1e3 * T_0)));

  vector<vektor> F_n;
  double Epot = 0;
  tie(F_n, Epot) = F(System.Teilchen);

  Teilchenliste tmp(System.N);

  std::map<string, std::unique_ptr<ofstream>> outputMap;
  vector<string> filenames = {"rx", "ry",   "vx", "vy",
                              "t",  "Temp", "E",  "v_ges"};
  for (auto fn : filenames) {
    outputMap[fn] = std::unique_ptr<ofstream>(new ofstream("build/" + Tzusatz + "_" + fn + ".txt"));
  }

  double t;
  double T_mean = 0;

  for (int step = 0; step < max_steps; step++) {
    tie(System.Teilchen, F_n, Epot) = Verlet_Step(System.Teilchen, F_n, h);

    t = step * h;
    if (t > t_eq) {
      System.update_histo();
      double Ekin = System.Ekin();
      double Tnow = Ekin / System.N;
      T_mean += Tnow;

      if (step % 10 == 0) {
        vektor v_ges = System.Schwerpunktsgeschwindigkeit();
        for (auto i : System.Teilchen) {
          *outputMap["rx"] << scientific << i.Ort.first << "\t";
          *outputMap["ry"] << scientific << i.Ort.second << "\t";
          *outputMap["vx"] << scientific << i.Geschwindigkeit.first << "\t";
          *outputMap["vy"] << scientific << i.Geschwindigkeit.second << "\t";
        }
        *outputMap["t"] << scientific << t;
        *outputMap["Temp"] << scientific << Tnow;
        *outputMap["E"] << scientific << Ekin << "\t" << Epot << "\t"
                        << Epot + Ekin;
        *outputMap["v_ges"] << scientific << v_ges.first << "\t" << v_ges.second;
        for (auto fn : filenames) *outputMap[fn] << endl;
      }
    }
  }
  ofstream Tmeanfile("build/" + Tzusatz + "_Tmean.txt");
  Tmeanfile << T_mean / MAX_STEPS;
  Tmeanfile.close();

  double L = System.Teilchen.at(0).L;
  double N = System.N;
  double cutoff = System.Teilchen.at(0).cutoff;
  const size_t bins = System.histogramm.size();
  ofstream histofile("build/" + Tzusatz + "_histogramm.txt");
  for (size_t i = 0; i < bins; i++) {
    double dV =
        M_PI * (pow(i + 1, 2.0) - pow(i, 2.0)) * pow(cutoff / bins, 2.0);
    System.histogramm[i] += L * L / (dV * N * N * MAX_STEPS);
    histofile << scientific << System.histogramm[i] << "\n";
  }

  for (auto fn : filenames) {
    outputMap[fn]->close();
  }
  histofile.close();
}

int main() {
  const double H = 0.001;
  const int N = 16;
  const int MAX_STEPS = 1e5;
  double t_eq = 10;
  const double L = 8;
  const double cutoff = 0.5 * L;
  const size_t bins = 500;

  cout << OUTPUT(MAX_STEPS);

  vector<double> Temperaturen = {0.01, 1.0, 2.0, 2.5};
  for (auto Temperatur : Temperaturen) {
    auto System = Konfiguration(N, L, cutoff, bins);
    System.init(Temperatur);
    Verlet(System, Temperatur, H, t_eq, MAX_STEPS);
  }

  double Temperatur = 0.2;
  auto System = Konfiguration(N, L, cutoff, bins);
  System.init(Temperatur);
  Verlet(System, Temperatur, 0.01, 0, 1e4);

  return 0;
}
