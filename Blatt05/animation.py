#!/usr/bin/env python
# encoding: utf-8
from __future__ import print_function, division

import numpy as np
import matplotlib.pyplot as plt
import seaborn
import matplotlib.animation as animation


L = 8
build_dir = 'build/'

def anim(Temp):
    path = build_dir + Temp + '_'
    rx = np.genfromtxt(path + 'rx.txt')
    ry = np.genfromtxt(path + 'ry.txt')
    vx = np.genfromtxt(path + 'vx.txt')
    vy = np.genfromtxt(path + 'vy.txt')
    N = len(rx)
    FFMpegWriter = animation.writers['ffmpeg']
    Writer = animation.writers['ffmpeg']
    writer = Writer(fps=15, metadata=dict(artist='Johannes Heuel'), bitrate=3600)
    fig = plt.figure(figsize=(10,10), dpi=100)
    #fig.set_size_inches(10,10)
    ax = fig.add_subplot(111, xlim=(0, L), ylim=(0, L))
    u = np.array([])
    x = ax.scatter(u, u, s=3000)
    v = ax.quiver(u, u, u, u)
    #ax.axis('off')
    line_ani = animation.FuncAnimation(fig, update_line, N, fargs=(x, v, ax, rx, ry, vx, vy),
            interval=10, blit=True)
    line_ani.save(path + 'anim.mp4', writer=writer, fps=60, dpi=100)
    #line_ani.save(path + 'anim.gif', writer='imagemagick', fps=30, dpi=90)

def update_line(i, x, v, ax, rx, ry, vx, vy):
    x.set_offsets(zip(rx[i], ry[i]))
    v.set_offsets(zip(rx[i], ry[i]))
    v.set_UVC(vx[i], vy[i])

def main():
    for T in [1000]:
        anim(str(T))

if __name__ == '__main__':
    main()
