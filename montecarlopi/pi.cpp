#include <iostream>
#include <fstream>
#include <cstdlib>

using namespace std;

const int N = 100000;

struct pointType {
    double x;
    double y;
};

pointType getRandom() {
    pointType p;
    p.x = 2.0 * rand() / RAND_MAX - 1;
    p.y = 2.0 * rand() / RAND_MAX - 1;
    return p;
}

int main()
{
    srand(1);

    int inCircle = 0;
    ofstream infile;
    ofstream outfile;

    infile.open("in.txt");
    outfile.open("out.txt");

    for (int i = 0; i < N; i++) {
        pointType p = getRandom();

        double r = p.x * p.x + p.y * p.y;
        if (r <= 1) {
            inCircle++;
            infile << p.x << "\t\t" << p.y << endl;
        } else {
            outfile << p.x << "\t\t" << p.y << endl;
        }
    }
    infile.close();
    outfile.close();
    double pi = 4 * (double) inCircle / N;
    std::cout << pi << endl;
}

