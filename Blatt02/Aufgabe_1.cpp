#include <iostream>
#include <functional>
#include <cmath>
#include <fstream>
using namespace std;


double Mittelpunktsregel(function<double(double)> f, double untereGrenze,
                         double obereGrenze, double Schrittweite)
{
    double E = 0;
    double i;
    for (i = untereGrenze; i < obereGrenze - Schrittweite; i+=Schrittweite) {
        E += f(i + Schrittweite/2) * Schrittweite;
    }
    if (i < obereGrenze) {
        E += f(i + (obereGrenze - i) / 2) * (obereGrenze - i);
    }
    return E;
}

double Simpsonregel(function<double(double)> f, double untereGrenze,
                    double obereGrenze, double Schrittweite)
{
    double E = 0;
    double i;
    for (i = untereGrenze; i < obereGrenze - Schrittweite; i+=Schrittweite) {
        E += Schrittweite / 6 * (f(i) + 4 * f(i + Schrittweite/2) + f(i + Schrittweite));
    }
    if (i < obereGrenze) {
        E += (obereGrenze - i) / 6 * (f(i) + 4 * f(i + (obereGrenze - i)/2) + f(i + (obereGrenze-i)));
    }
    return E;
}

double Hauptwertintegral(function<double(double)> f, double untereGrenze,
                         double obereGrenze, double Schrittweite, double z)
{
    double E = 0;
    double delta = 2 * Schrittweite;
    function<double(double)> integrand = [f, z](double x) { return f(x) / (x - z); };
    if (untereGrenze < z)
        E += Simpsonregel(integrand, untereGrenze, z - delta, Schrittweite);

    function<double(double)> g = [f, delta, z](double s) { return ( f(s*delta + z) - f(z) ) / s; };
    // hier wird die Mittelpunktsregel verwendet, aufgrund der problematischen Randpunkte
    E += Mittelpunktsregel(g, z - delta, z, Schrittweite);
    E += Mittelpunktsregel(g, z, z + delta, Schrittweite);

    if (obereGrenze > z)
        E += Simpsonregel(integrand, z + delta, obereGrenze, Schrittweite);
    return E;
}



int main()
{
    // Aufgabenteil a)
    double untereGrenze = -1;
    double obereGrenze = 1;
    double Schrittweite = 1e-7;
    double z = 0;
    double Ergebnis_a = Hauptwertintegral(
            [](double x) { return exp(x); },
            untereGrenze,
            obereGrenze,
            Schrittweite,
            z);


    // Aufgabenteil b)

    // Substitution: u = sqrt(t)
    double Ergebnis_b = 0;
    Ergebnis_b += Mittelpunktsregel(
            //[](double x) { return 2 * exp(-x*x); },
            [](double x) { return exp(-x)/sqrt(x); },
            0,
            0.001,
            1e-10);
    Ergebnis_b += Simpsonregel(
            [](double x) { return exp(-x)/sqrt(x); },
            0.001,
            12,
            1e-4);


    // exaktes Ergebnis: sqrt(pi)
    double exakt = sqrt(M_PI);
    ofstream f;
    f.open("build/Aufgabe_1.tex");
    char str[100];
    sprintf(str, "I_1 &= %.6f\\\\\n", Ergebnis_a);
    f << str;
    sprintf(str, "I_2 &= %.8f\\\\\n \\Delta I_2 &= %.7f\\\\\n",
            Ergebnis_b,
            abs((Ergebnis_b - exakt)/exakt));
    f << str;
    f.close();
    return 0;
}
