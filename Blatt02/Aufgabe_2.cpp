#define OUTPUT(x) #x << " = " <<  (x)
#include <iostream>
#include <cmath>
#include <functional>
#include <fstream>
using namespace std;

const double a = 1;
const double Rho0 = 1;
const double epsilon_0 = 8.854187817 * 1e-12;
const double vorfaktor = 1;//Rho0 * a*a / (4 * M_PI * epsilon_0);
const double h = 0.1;

double Mittelpunktsregel(function<double(double)> f, double untereGrenze,
                         double obereGrenze, double Schrittweite)
{
    double E = 0;
    double i;
    for (i = untereGrenze; i < obereGrenze - Schrittweite; i+=Schrittweite) {
        E += f(i + Schrittweite/2) * Schrittweite;
    }
    if (i < obereGrenze) {
        E += f(i + (obereGrenze - i) / 2) * (obereGrenze - i);
    }
    return E;
}

double Simpsonregel(function<double(double)> f, double untereGrenze,
                    double obereGrenze, double Schrittweite)
{
    double E = 0;
    double i;
    for (i = untereGrenze; i < obereGrenze - Schrittweite; i+=Schrittweite) {
        E += Schrittweite / 6 * (f(i) + 4 * f(i + Schrittweite/2) + f(i + Schrittweite));
    }
    if (i < obereGrenze) {
        E += (obereGrenze - i) / 6 * (f(i) + 4 * f(i + (obereGrenze - i)/2) + f(i + (obereGrenze-i)));
    }
    return E;
}

double Integral3D(function<double(double, double, double, double)> f, double x)
{
    double Ergebnis = Mittelpunktsregel(
            [&](double X) { return Mittelpunktsregel(
                [&](double Y) { return Mittelpunktsregel(
                    [&](double Z) { return f(x, X, Y, Z); },
                    -1, 1, h);},
                -1, 1, h);},
            -1, 1, h);
    return Ergebnis;
}

int main()
{
    double x;
    ofstream f;
    f.open("Aufgabe_2_a.txt");

    auto rho1 = [](double x, double X, double Y, double Z)
    {
        double R = (x-X)*(x-X) + Y*Y + Z*Z;
        if (R < 1e-6) return 0.0;
        else return 1 / sqrt(R);
    };

    auto rho2 = [](double x, double X, double Y, double Z)
    {
        double R = (x-X)*(x-X) + Y*Y + Z*Z;
        if (R < 1e-6) return 0.0;
        else return X / sqrt(R);
    };

    for (int n = 0; n <= 800; n++)
    {
        x = 0.01 * n;
        f   << x
            << "\t\t" << vorfaktor * Integral3D(rho1, x)
            << "\t\t" << 8 * vorfaktor * 1/x
            << "\t\t" << vorfaktor / a * Integral3D(rho2, x)
            << "\t\t" << 8 / 3 * vorfaktor / a * 1/(x*x)
            << endl;
    }
    f.close();

    return 0;
}
