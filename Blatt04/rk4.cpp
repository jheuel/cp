#include "vektor.cpp"
#include <vector>
#include <functional>

extern const double G;
extern const double I;

double F(double theta, vektor p);

vektor RK4(std::function<vektor(double t, vektor r)> F, double dt, double t, vektor R)
{
    vektor k_1 = dt * F(t, R);
    vektor k_2 = dt * F(t + 0.5 * dt, R + 0.5 * k_1);
    vektor k_3 = dt * F(t + 0.5 * dt, R + 0.5 * k_2);
    vektor k_4 = dt * F(t + dt, R + k_3);

    vektor Ergebnis = R + 1 / 6.0f * (k_1 + 2*k_2 + 2*k_3 + k_4); // + O(dt^5)
    return Ergebnis;
}

std::vector<vektor> Newton(vektor start, int N, double h, std::function<vektor(double)> p)
{
    std::vector<vektor> theta_t;
    auto f = [p](double t, vektor T) {
        return std::make_pair(T.second,
                (- G * T.second + F(T.first, p(t))) / I);
    };

    theta_t.push_back(start);

    for (int i = 0; i < N; i++)
    {
        theta_t.push_back(RK4(f, h, i * h, theta_t.back()));
    }
    return theta_t;
}
