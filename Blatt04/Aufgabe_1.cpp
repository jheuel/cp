#include <iostream>
#include <vector>
#include <cmath>
#include <functional>
#include <fstream>
#include <sstream>
#include "vektor.cpp"
#include "rk4.cpp"
#include "physik.cpp"

const double A = 0.2;       // m
const double B = 0.1;       // m
const double G = 2e-4;      // kg m^2 s^(-1)
const double I = 6250e-7;   // kg m^2
const double K = 0.25;      // kg s^(-2)
const double R = 5e-2;      // m

void Aufgabenteil_a(std::string filename, vektor p)
{
    double von = 0;
    double bis = 2 * M_PI;
    int N = 500;
    double h = (bis - von) / N;
    std::stringstream s;
    s << filename
        << "_" << 100 * p.first
        << "_" << 100 * p.second
        << ".txt";
    std::ofstream ausgabe;
    ausgabe.open(s.str());
    for (int i = von; i < N; i++)
    {
        ausgabe
                << i * h
                << "\t"
                << V(i * h, p)
                << "\t"
                << -Ableitung([p](double theta) { return V(theta, p); }, i * h, h)
                << "\t"
                << F(i * h, p)
                << "\n";
    }
    ausgabe.close();
}

void Aufgabenteil_b(vektor p, std::string filename)
{
    double von = 0;
    double bis = 2 * M_PI;
    int N = 500;
    double h = (bis - von) / N;

    std::stringstream s;
    s << filename
        << "_" << 100 * p.first
        << "_" << 100 * p.second
        << ".txt";
    std::ofstream ausgabe;
    ausgabe.open(s.str());
    for (int i = von; i < N; i++)
    {
        ausgabe
                << i * h
                << "\t"
                << V(i * h, p)
                << "\t"
                << F(i * h, p)
                << "\n";
    }
    ausgabe.close();
}

void Aufgabenteil_b2(vektor start, vektor p, std::string filename)
{
    int N = 10000;
    double h = 0.01;
    auto p_t = [p](double t) { return p; };
    std::vector<vektor> theta_t = Newton(start, N, h, p_t);

    std::stringstream s;
    s   << filename
        << "_" << 100 * p.first
        << "_" << 100 * p.second
        << "_" << start.first
        << "_" << start.second
        << ".txt";
    std::ofstream ausgabe;
    ausgabe.open(s.str());
    for (size_t i = 0; i < theta_t.size(); i+=10)
    {
        ausgabe
            << i * h
            << "\t"
            << theta_t[i].first
            << "\t"
            << theta_t[i].second
            << "\t"
            << 100 * p.first
            << "\t"
            << 100 * p.second
            << "\n";
    }
    ausgabe.close();
}

vektor Aufgabenteil_c(vektor start, std::function<vektor(double)> p_t, std::string filename)
{
    double von = 0;
    double bis = 100;
    int N = 10000;
    double h = (bis - von) / N;
    std::vector<vektor> theta_t = Newton(start, N, h, p_t);

    std::stringstream s;
    s   << filename
        << "_" << 100 * p_t(0).first
        << "_" << 100 * p_t(0).second
        << ".txt";
    std::ofstream ausgabe;
    ausgabe.open(s.str());
    for (size_t i = 0; i < theta_t.size(); i++)
    {
        ausgabe
            << i * h
            << "\t"
            << theta_t[i].first
            << "\t"
            << theta_t[i].second
            << "\n";
    }
    ausgabe.close();
    return theta_t.back();
}

int main()
{
    vektor p = std::make_pair(0.18, 0.01);

    Aufgabenteil_a("build/a", p);

    std::vector<vektor> Randbed = {
        std::make_pair(.16, .00),
        std::make_pair(.16, .02)
    };

    std::vector<vektor> startwerte = {
        std::make_pair(2, 0),
        std::make_pair(4, 0)
    };

    for (auto r : Randbed)
    {
        Aufgabenteil_b(r, "build/b");
        for (auto start : startwerte)
        {
            Aufgabenteil_b2(start, r, "build/b");
        }
    }

    auto p1 = [](double t)
    {
        return std::make_pair(0.16, 0.02/100 * t);
    };

    auto p2 = [](double t)
    {
        return std::make_pair(0.16, 0.02 - 0.02/100 * t);
    };

    vektor gg = std::make_pair(3.88, 0);
    gg = Aufgabenteil_c(gg, p1, "build/c");
    Aufgabenteil_c(gg, p2, "build/c2");

    return 0;
}
