#include <fstream>
#include <functional>
#include <iostream>
#include <sstream>
#include <vector>
#include "vektor.cpp"
#include "physik.cpp"
#include "rk4.cpp"

const double OMEGA = 2 * M_PI / 10;     // s^(-1)
const double A = 0.2;                   // m
const double B = 0.1;                   // m
const double G = 2e-4;                  // kg m^2 s^(-1)
const double I = 6250e-7;               // kg m^2
const double K = 0.25;                  // kg s^(-2)
const double R = 0.05;                  // m
const double A2 = 0.025;

vektor f(double t, double x)
{
    return std::make_pair(x, A2 * sin(OMEGA * t));
}

void Ausgabe(std::vector<vektor> theta_t, double h, std::string filename, std::function<vektor(double)> p_t)
{
    std::ofstream ausgabe;
    ausgabe.open(filename);
    for (size_t j = 0; j < theta_t.size(); j+=1)
    {
        auto p = p_t(j * h);
        ausgabe
            << j * h
            << "\t"
            << theta_t[j].first
            << "\t"
            << theta_t[j].second
            << "\t"
            << 100 * p.first
            << "\t"
            << 100 * p.second
            << "\n";
    }
    ausgabe.close();
}

void Aufgabenteil_a()
{
    std::vector<double> x = {
        0.218,
        0.219,
        0.21925,
        0.2195,
        0.22,
        0.2225
    };

    vektor start = std::make_pair(0, 0);

    double von = 0;
    double bis = 200;
    int N = 10000;
    double h = (bis - von) / N;

    for (auto i : x)
    {
        auto p_t = [i](double t) { return f(t, i); };
        std::vector<vektor> theta_t = Newton(start, N, h, p_t);
        std::stringstream s;
        s   << "build/2a"
            << "_" << 1e5 * i
            << ".txt";
        Ausgabe(theta_t, h, s.str(), p_t);
    }
}

void Aufgabenteil_b(double x)
{
    vektor start = std::make_pair(0, 0);

    double von = 0;
    double bis = 1000;
    double dt = 4000;
    double h = 2 * M_PI / OMEGA / dt;
    int N = ceil((bis - von) / h);

    auto p_t = [x](double t) { return f(t, x); };
    std::vector<vektor> theta_t = Newton(start, N, h, p_t);
    std::stringstream s;
    s   << "build/2b"
        << "_" << 100 * x
        << ".txt";
    std::ofstream ausgabe;
    ausgabe.open(s.str());
    for (size_t j = 10 * dt; j < theta_t.size() && j < 100 * dt; j += dt)
    {
        ausgabe
            << theta_t[j].first
            << "\t"
            << theta_t[j].second
            << "\n";
    }
    ausgabe.close();
}

void Aufgabenteil_c()
{
    vektor start = std::make_pair(0, 0);

    double von = 0;
    double bis = 1000;
    double dt = 100;
    double h = 2 * M_PI / OMEGA / dt;
    int N = ceil((bis - von) / h);
    double dx = 0.00001;

    std::vector<vektor> bifurk;
    for (double x = 0.218; x < 0.22; x += dx)
    {
        auto p_t = [x](double t) { return f(t, x); };
        std::vector<vektor> theta_t = Newton(start, N, h, p_t);
        std::ofstream ausgabe;
        for (size_t j = 10 * dt; j < theta_t.size() && j < 100 * dt; j += dt)
        {
            bifurk.push_back(
                    std::make_pair(x, theta_t[j].first));
        }
    }
    std::ofstream ausgabe;
    ausgabe.open("build/2c.txt");
    for (size_t j = 0; j < bifurk.size(); j++)
    {
        ausgabe
            << bifurk[j].first
            << "\t"
            << bifurk[j].second
            << "\n";
    }
    ausgabe.close();
}

int main()
{
    Aufgabenteil_a();
    Aufgabenteil_b(0.22);
    Aufgabenteil_c();
    return 0;
}
