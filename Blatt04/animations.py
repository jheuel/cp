#!/usr/bin/env python
# encoding: utf-8

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation

pi = 3.1415
R = 5
a = 20
A = (-a, 0)
b = 10
k = 250
I = 6250
gamma = 2000
FFMpegWriter = animation.writers['ffmpeg']
Writer = animation.writers['ffmpeg']
writer = Writer(fps=15, metadata=dict(artist='Johannes Heuel'), bitrate=1800)

def update_line(n, l, r, t, theta_t, p):
    l.set_data(*zip(A, Kreisbogen(theta_t[n])))
    r.set_data(*zip(Kreisbogen(theta_t[n]), p[n]))

def Kreisbogen(theta):
    return [ R * np.cos(theta), R * np.sin(theta) ]

for i in ['2a_21800', '2a_21900', '2a_21925', '2a_21950', '2a_22000', '2a_22250']:
    print(i)
    build_dir = 'build/'
    filename = build_dir + i
    t, theta_t, omega, px, py = np.genfromtxt(filename + '.txt', unpack=True)
    p = zip(px, py)

    fig = plt.figure()
    lim = max(a, px.max())
    ax = fig.add_subplot(111, xlim=(-lim, lim + 1), ylim=(-0.75* lim, 0.75*(lim + 1)))
    l, = ax.plot([], [], '-o', color='black')
    r, = ax.plot([], [], '-o', color='black')
    umlauf = np.linspace(0, 2 * pi, 100)
    k, = ax.plot(*Kreisbogen(umlauf), color='blue')
    ax.axis('off')
    line_ani = animation.FuncAnimation(fig, update_line, len(t), fargs=(l, r, t, theta_t, p),
            interval=60, blit=True)
    line_ani.save(filename + '.mp4')
