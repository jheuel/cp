#ifndef __VEKTOR__
#define __VEKTOR__
#include <cmath>
#include <iostream>

using vektor = std::pair<double, double>;

double operator*(const vektor& a, const vektor& b)
{
    return a.first * b.first + a.second * b.second;
}

vektor operator*(const double a, const vektor& b)
{
    return std::make_pair(a * b.first, a * b.second);
}

vektor operator*(const vektor& a, const double b)
{
    return b * a;
}

vektor operator+(const vektor& a, const vektor& b)
{
    return std::make_pair(a.first + b.first, a.second + b.second);
}

vektor operator-(const vektor& a, const vektor& b)
{
    return std::make_pair(a.first - b.first, a.second - b.second);
}

double Betrag(const vektor& a)
{
    return sqrt(a*a);
}
#endif
