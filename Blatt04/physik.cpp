#ifndef __PHYSIK__
#define __PHYSIK__

#include <iostream>
#include "vektor.cpp"

extern const double A;
extern const double B;
extern const double G;
extern const double K;
extern const double R;

double Ableitung(std::function<double(double)> f, double theta, double h)
{
    return ( f(theta + h) - f(theta - h) )
                    / (2 * h);
}

double d_1(double theta)
{
    return Betrag(std::make_pair(-A, 0)
            - R * std::make_pair(cos(theta), sin(theta))
            );
}

double d_2(double theta, vektor p)
{
    return Betrag(
            R * std::make_pair(cos(theta), sin(theta)) - p
            );
}

double V(double theta, vektor p)
{
    return 0.5 * K * pow((d_1(theta) - B), 2)
         + 0.5 * K * pow((d_2(theta, p) - B), 2);
}

double F(double theta, vektor p)
{
    double x = p.first;
    double y = p.second;
    double z1 = - A * R * sin(theta);
    double z2 = - R * (y * cos(theta) - x * sin(theta));
    return -K * (z1 - B * z1 / d_1(theta)) - K * (z2 - B * z2 / d_2(theta, p));
}

#endif
