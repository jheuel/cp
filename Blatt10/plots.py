#!/usr/bin/env python
# encoding: utf-8

import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
import seaborn as sns
import scipy.optimize as opt
import pandas as pd
sns.set(context="paper")

def f(d, t):
    return 4 * d * t

def Aufgabe1():
    data = np.genfromtxt('build/1.txt', unpack=True)
    mydpi = 300

    plt.figure()
    plt.plot(data[0], data[1], ',', alpha=0.2)
    plt.ylabel(r"$\langle y\rangle$")
    plt.xlabel(r"$\langle x\rangle$")
    plt.savefig("build/rw.png", dpi=mydpi)

    timesteps = [10, 50, 100, 500, 1000]
    steps = len(data[0])
    for ts in timesteps:
        t = np.linspace(0, steps / ts, steps / ts)
        par, _ = opt.curve_fit(f, t, data[2][::ts])
        plt.figure()
        plt.plot(t, data[2][::ts], ',')
        plt.plot(t, f(par[0], t), '-k')
        plt.ylabel(r"$\langle r^2\rangle$")
        plt.xlabel("$t$")
        plt.title("D= " + str(par[0] / ts))
        plt.savefig("build/r2_" + str(ts) + ".png", dpi=mydpi)

    timesteps = [10, 100, 1000]
    for ts in timesteps:
        #plt.figure()
        #plt.hist2d(
                #data[0][::ts], data[1][::ts], bins=150, normed=True, cmap="Blues")
        #y = stats.gamma(5).rvs(5000)
        with sns.axes_style("white"):
            fig = plt.figure()
            sns.set_style("ticks")
            g = sns.jointplot(data[0][::ts], data[1][::ts], stat_func=None, kind="hex")
            plt.ylabel(r"$\langle y\rangle$")
            plt.xlabel(r"$\langle x\rangle$")

            plt.savefig("build/hist_" + str(ts) + ".png", dpi=mydpi)


def Aufgabe2b():
    data = np.genfromtxt("build/2b.txt", unpack=True)
    fig, ax = plt.subplots()
    ax.plot(data[0], data[1], "o")
    ax.set_yscale('log')
    ax.set_xscale('log')
    ax.set_xlim(1, 10**7)
    ax.set_xlabel("$N$")
    ax.set_ylabel("relativer Fehler")
    fig.savefig("build/2b.pdf")

    data = np.genfromtxt("build/2b2.txt", unpack=True)
    fig, ax = plt.subplots()
    ax.hist(data, alpha=0.7, bins=20)
    #ax.hist(data, histtype="stepfilled", alpha=0.7, bins=20)
    ax.set_xlabel("Wert")
    ax.set_ylabel("Anzahl")
    fig.savefig("build/2b2.pdf")

def Aufgabe2c():
    x, y, z = np.genfromtxt("build/2c.txt", unpack=True)
    fig = plt.figure()
    ax = Axes3D(fig)
    #ax = fig.gca(projection='3d')
    ax.plot(x, y, z, '.')
    ax.set_xlabel("$a$")
    ax.set_ylabel("$b$")
    ax.set_zlabel(u"Fläche")
    #ax.plot_surface(X, Y, Z, rstride=1, cstride=1, cmap=cm.coolwarm,
        #linewidth=10, antialiased=False)

    fig.savefig("build/2c.pdf")

def main():
    Aufgabe1()
    Aufgabe2b()
    Aufgabe2c()


if __name__ == '__main__':
    main()
