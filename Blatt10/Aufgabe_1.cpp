#include <fstream>
#include <iostream>
#include <random>

using std::cout;
using std::endl;
using std::inner_product;
using std::accumulate;
using std::ofstream;
using std::vector;

int main() {
  const double a = 1e-2;
  const size_t N = 1000;
  const unsigned int nRandomWalks = 1000000;

  //long long seed(12345678912345678912ull);
  std::mt19937_64 mt;
  std::uniform_real_distribution<double> dist(
      -a, std::nextafter(a, std::numeric_limits<double>::max()));
  auto rnd = [&dist, &mt]() { return dist(mt); };

  // random numbers
  vector<double> xdat(N, 0);
  vector<double> ydat(N, 0);

  // result
  vector<double> x(nRandomWalks, 0);
  vector<double> y(nRandomWalks, 0);
  vector<double> r2(N, 0);

  for (size_t i = 0; i < x.size(); i++) {
    for (size_t j = 0; j < xdat.size(); j++) {
      xdat[j] += rnd();
      ydat[j] += rnd();
    }
    x[i] = accumulate(xdat.begin(), xdat.end(), 0.) / (N * a);
    y[i] = accumulate(ydat.begin(), ydat.end(), 0.) / (N * a);

    r2[i] =
        inner_product(xdat.begin(), xdat.end(), xdat.begin(), 0.) /
            (N * a * a) +
        inner_product(ydat.begin(), ydat.end(), ydat.begin(), 0.) / (N * a * a);
  }

  ofstream output("build/1.txt");
  for (size_t i = 0; i < x.size(); i++) {
    output << x[i] << "\t" << y[i] << "\t" << r2[i] << endl;
  }
  output.close();

  return 0;
}
