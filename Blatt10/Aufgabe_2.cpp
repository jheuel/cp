#include <cmath>
#include <fstream>
#include <functional>
#include <iostream>
#include <random>

using std::cout;
using std::endl;
using std::function;
using std::ofstream;

long double
integrate2D(const int N, double a = 1, double b = 1,
            function<double(double, double)> func = nullptr, double zmax = 1) {
  if (func == nullptr) {
    func = [zmax](double x, double y) {
      // remove unused parameter warning
      (void)x;
      (void)y;
      return 1;
    };
  }
  static long long seed(12345678912345678912ull);
  static std::mt19937_64 mt(seed);
  std::uniform_real_distribution<double> dista(0, a);
  std::uniform_real_distribution<double> distb(0, b);
  long double hit = 0;
  for (int i = 0; i < N; i++) {
    double x = dista(mt);
    double y = distb(mt);
    if (pow(x / a, 2) + pow(y / b, 2) < 1) {
      hit += func(x, y);
    }
  }
  return hit * 4 * a * b / N;
}

void a_und_b() {
  ofstream output("build/2b.txt");
  for (int k = 1; k < 7; k++) {
    int N = pow(10, k);
    double pi = integrate2D(N);
    cout << pi << endl;
    double fehler = fabs(pi - M_PI) / M_PI;
    output << N << "\t" << fehler << endl;
  }
  output.close();

  const int N = 1000;
  output.open("build/2b2.txt");
  for (int i = 0; i < N; i++) {
    output << integrate2D(N) << endl;
  }
  output.close();
}

void c() {
  ofstream output("build/2c.txt");
  int N = 100;
  double da = 0.01;
  double db = 0.01;

  for (int i = 0; i < N; i++) {
    double a = i * da;
    for (int j = 0; j < N; j++) {
      double b = j * db;
      output << a << "\t"             //
             << b << "\t"             //
             << integrate2D(N, a, b)  //
             << endl;
    }
  }
  output.close();
}

void d() {
  ofstream output("build/2d.txt");
  output.precision(8);
  int N = 1e9;
  const double a = sqrt(2);
  const double b = 1;

  function<double(double x, double y)> func = [](double x, double y) {
    // unused parameter warning
    (void)y;
    return exp(-x * x);
  };

  auto Ergebnis = integrate2D(N, a, b, func);
  cout << Ergebnis << endl;
  output << Ergebnis << endl;
  output.close();
}

int main() {
  a_und_b();
  c();
  d();
  return 0;
}
