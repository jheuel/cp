#include <iostream>
#include <cmath>

using namespace std;


double Trapezregel(double (*f)(double x), double untereGrenze, double obereGrenze, double h){
	double E = 0;
	double i;
	for (i = untereGrenze; i < obereGrenze-h; i+=h){
		E+=((*f)(i) + (*f)(i + h)) / 2 * h;
	}
	if (i < obereGrenze){
		E += ((*f)(i)+(*f)(obereGrenze)) / 2 * (obereGrenze - i);
	}
	return E;
}

double Mittelpunksregel(double(*f)(double x), double untereGrenze, double obereGrenze, double h){
	double E = 0;
	double i;
	for (i = untereGrenze; i < obereGrenze-h; i+=h){
		E += (*f)(i + h / 2)* h;
	}
	if (i < obereGrenze){
		E += (*f)(i + (obereGrenze - i) / 2)* (obereGrenze - i);
	}
	return E;
}

double Simpsonregel(double (*f)(double x), double untereGrenze, double obereGrenze, double h){
	double E = 0;
	double i;
	for (i = untereGrenze; i < obereGrenze-h; i+=h){
		E += h / 6 * ((*f)(i)+4 * (*f)(i + h / 2) + (*f)(i + h));
	}
	if (i < obereGrenze){
		E += (obereGrenze - i) / 6 * ((*f)(i)+4 * (*f)(i + (obereGrenze - i) / 2) + (*f)(i + (obereGrenze - i)));
	}
	return E;
}
double f(double x){ return exp(-x) / x; }
double g(double x){ return x* sin(1 / x); }

int main(){
	double U1 = 1;
	double O1 = 100;
	double h1 = 0;
	cout << "Schrittweite eingeben	";
	cin >> h1; cout << endl;
	if (h1 > O1 - U1){
		cout << "Schritt zu gross, neu eingeben	" << endl;
		cin >> h1; cout << endl;
	}
	for (int Genug = 0; Genug < 1;){
		cout << "Trapezregel	" << Trapezregel(&f, U1, O1, h1) << endl;
		cout << "Mittelpunksregel	" << Mittelpunksregel(&f, U1, O1, h1) << endl;
		cout << "Simpsonregel	" << Simpsonregel(&f, U1, O1, h1) << endl <<endl ;
		h1 /= 2;
		cin >> Genug;
	}

	double U2 = 0;
	double O2 = 1;
	double h2 = 0;
	cout << "Schrittweite eingeben	";
	cin >> h2; cout << endl;
	if (h2 > O2 - U2){
		cout << "Schritt zu gross, neu eingeben	" << endl;
		cin >> h1; cout << endl;
	}
	for (int Genug = 0; Genug < 1;){

		cout << "Trapezregel	" << Trapezregel(&g, U2+h2, O2, h2) << endl;
		cout << "Mittelpunksregel	" << Mittelpunksregel(&g, U2+h2, O2, h2) << endl;
		cout << "Simpsonregel	" << Simpsonregel(&g, U2+h2, O2, h2) << endl;
		h2 /= 2;
		cin >> Genug;
	}
	return 0;
}
