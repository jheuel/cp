#include <iostream>
#include <fstream>
using namespace std;
#include <cmath>


const double M = 1;
const double a = 1;
const double vorfaktor = 1e-7; //mu0 / (4 * M_PI);

struct Vektor {
    double x;
    double y;
};

double E(int N, double theta, bool ferromagnetisch)
{
    double Energie = 0;

    for (int k = -N; k <= N; k++)
        for (int l = -N; l <= N; l++)
        {
            // Ursprung ueberspringen
            if ( k == 0 && l == 0 )
                continue;

            int faktorFerromagnetisch = 1;
            if (!ferromagnetisch)
                if ((l+k) % 2 != 0)
                    faktorFerromagnetisch = -1;

            double R = sqrt( k*k + l*l );
            double mx = M * sin(theta);
            double my = M * cos(theta);
            Energie += faktorFerromagnetisch / (a*a*a*R*R*R) *                  \
                      ( - 3.0 * ( l * mx + k * my ) * ( k * M ) / (R*R)         \
                        + ( my * M )                                            \
                      );
        }
    return vorfaktor * Energie;
}

Vektor B(int N, bool ferromagnetisch)
{
    Vektor Magnetfeld;
    Magnetfeld.x = 0;
    Magnetfeld.y = 0;

    for (int k = -N; k <= N; k++)
        for (int l = -N; l <= N; l++)
        {
            // Ursprung ueberspringen
            if ( k == 0 && l == 0 )
                continue;

            int faktorFerromagnetisch = 1;
            if (!ferromagnetisch)
                if ((l+k) % 2 != 0)
                    faktorFerromagnetisch = -1;

            double R = sqrt( k*k + l*l );

            // m = M * (0 1)
            Magnetfeld.y += faktorFerromagnetisch / (a*a*a*R*R*R) * (       \
                                + 3.0 * (l * l * M) / (R*R)                 \
                                - M                                         \
                                );
        }
    Magnetfeld.x *= vorfaktor;
    Magnetfeld.y *= vorfaktor;
    return Magnetfeld;
}

int main(int argc, char const *argv[])
{
    int n[] = {2, 5, 10};
    int length = sizeof(n) / sizeof(n[0]);
    int steps = 500;

    double ferro[length][steps];
    double antiferro[length][steps];
    double theta[length][steps];

    for (int j = 0; j < length; j++)
    {
        int N = n[j];
        ofstream f;
        char filename[20];
        sprintf(filename, "Aufgabe_1_%d.dat", N);
        f.open(filename);
        f << "# theta_0 \t\t Energie" << endl;
        f.setf(ios_base::scientific);
        for (int i = 0; i < steps; i++)
        {
            theta[j][i] = i * 2 * M_PI / steps;
            ferro[j][i] = E(N, theta[j][i], true);
            antiferro[j][i] = E(N, theta[j][i], false);
            f << theta[j][i] << "\t" << ferro[j][i] << "\t" << antiferro[j][i] << endl;
        }
        f.close();
    }


    double Tferro[length][steps];
    double Tantiferro[length][steps];
    double Bferro[length][steps];
    double Bantiferro[length][steps];
    for (int j = 0; j < length; j++)
    {
        int N = n[j];
        ofstream f;
        char filename[20];
        sprintf(filename, "Aufgabe_1_b_%d.dat", N);
        f.open(filename);
        f << "# theta_0 \t\t Drehmoment via Ableitung \t\t Drehmoment via Drehmomente" << endl;
        f.setf(ios_base::scientific);
        for (int i = 1; i < steps-1; i++)
        {
            Tferro[j][i] = (ferro[j][i-1] - ferro[j][i+1]) / (4 * M_PI /steps);
            Tantiferro[j][i] = (antiferro[j][i-1] - antiferro[j][i+1]) / (4 * M_PI /steps);
            Vektor Bf = B(N, true);
            Bferro[j][i] = M * sin(theta[j][i]) * Bf.y - M * cos(theta[j][i]) * Bf.x;
            Vektor Baf = B(N, false);
            Bantiferro[j][i] = M * sin(theta[j][i]) * Baf.y - M * cos(theta[j][i]) * Baf.x;
            f << theta[j][i] << "\t" << Tferro[j][i] << "\t" << Tantiferro[j][i];
            f << "\t" << Bferro[j][i] << "\t" << Bantiferro[j][i] << endl;
        }
        f.close();
    }

    return 0;
}
