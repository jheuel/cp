#include <iostream>
#include <vector>
#include <functional>
#include <fstream>
#include <sstream>
#include <cstdlib>

using std::vector;
using std::function;
using std::make_pair;
using std::endl;
using vektor = std::pair<double, double>;

void Ausgabe(const vector<vector<vektor>> &A, std::string Aufgabenteil) {
  std::string filename;
  filename = "build/1_" + Aufgabenteil + ".txt";
  std::ofstream outfile(filename);
  for (auto i : A) {
    for (auto j : i) {
      outfile << j.first << "\t\t" << j.second << endl;
    }
  }
  outfile.close();
}

vector<vector<vektor>>
iterate(const size_t rsteps, const double r_min, const double r_max,
        const size_t stepstoorbit, const size_t orbitsteps,
        const function<double(double, double)> f) {
  vector<vector<vektor>> Ergebnis(rsteps);

  const double dr = (r_max - r_min) / rsteps;
  double r = r_min;
  double x_n, x_np;
  srand(rsteps + r_min + r_max + orbitsteps);
  for (size_t i = 0; i < rsteps; i++) {
    r += dr;
    x_np = static_cast<double>(2. * rand()) / static_cast<double>(RAND_MAX) - 1.;
    for (size_t j = 0; j < stepstoorbit + orbitsteps; j++) {
      x_n = x_np;
      x_np = f(r, x_n);
      if (j > stepstoorbit) {
        Ergebnis[i].push_back(make_pair(r, x_np));
      }
    }
  }
  return Ergebnis;
}

int main() {
  const size_t rsteps = 1000;
  const size_t stepstoorbit = 100;
  const size_t orbitsteps = 300;

  const double r1_min = 0;
  const double r1_max = 4.0;

  const double r2_min = 0;
  const double r2_max = 3;

  const auto f1 = [](double r, double x) { return r * x * (1 - x); };
  const auto f2 = [](double r, double x) { return r * x - x * x * x; };

  vector<vector<vektor>> data1 =
      iterate(rsteps, r1_min, r1_max, stepstoorbit, orbitsteps, f1);
  vector<vector<vektor>> data2 =
      iterate(rsteps, r2_min, r2_max, stepstoorbit, orbitsteps, f2);

  Ausgabe(data1, "a");
  Ausgabe(data2, "b");

  return 0;
}
