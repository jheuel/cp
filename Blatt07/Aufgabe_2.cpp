#include <vector>
#include <functional>
#include <fstream>
#include <cmath>
#include <iostream>

double f(const double r, const double x) { return r * x * (1. - x); }

double f_n(double r, double x, size_t n) {
  if (n == 1) {
    return f(r, x);
  } else {
    return f(r, f_n(r, x, n - 1));
  }
}
std::tuple<double, double>
bisection_step(const std::function<double(double)> &f, double x_0, double y_0) {
  double z_1 = 0.5 * (x_0 + y_0);
  double fz = f(z_1);
  if (fz == 0)
    return std::make_tuple(z_1, z_1);
  else if (f(x_0) * fz < 0)
    return std::make_tuple(x_0, z_1);
  else
    return std::make_tuple(z_1, y_0);
}

double bisection(const std::function<double(double)> &f, double x_0, double y_0,
                 double eps = 1e-15) {
  while (y_0 - x_0 > eps) {
    std::tie(x_0, y_0) = bisection_step(f, x_0, y_0);
    if (x_0 == y_0) return x_0;
  }
  return 0.5 * (y_0 + x_0);
}

double g_n(double r, double x, size_t n) { return 0.5 - f_n(r, x, n); }

void Aufgabenteil_a() {
  const double r_inf = 3.569946;
  const double r_min = 0;
  const double r_max = r_inf;
  const size_t steps = 1000;
  const double dr = (r_max - r_min) / steps;
  double r = r_min;

  std::ofstream outfile("build/2_a.txt");
  for (size_t i = 0; i < steps; i++) {
    outfile << r << "\t";
    for (size_t j = 0; j < 4; j++) {
      int exponent = pow(2, j);
      outfile << g_n(r, 0.5, exponent) << "\t";
    }
    outfile << "\n";
    r += dr;
  }
  outfile.close();
}

void Aufgabenteil_b() {
  const double r_inf = 3.569946;
  double x = 0.5;
  auto g_0 = [x](double r) { return g_n(r, x, 1); };
  auto g_1 = [x](double r) { return g_n(r, x, 2); };
  auto g_2 = [x](double r) { return g_n(r, x, 4); };
  auto g_3 = [x](double r) { return g_n(r, x, 8); };

  double R_0 = bisection(g_0, 1.5, 2.3);
  double R_1 = bisection(g_1, 3, 3.3);
  double R_2 = bisection(g_2, 3.3, 3.5);
  double R_3 = bisection(g_3, 3.5, r_inf);
  std::cout.precision(12);
  std::cout << "Nullstellen:\n" << R_0 << "  " << R_1 << "  " << R_2 << "  "
            << R_3 << std::endl;
  std::cout << "Feigenbaum:  " << (R_2 - R_1) / (R_3 - R_2) << std::endl;
}

int main() {
  Aufgabenteil_a();
  Aufgabenteil_b();
  return 0;
}
