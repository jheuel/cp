#!/usr/bin/env python
# encoding: utf-8

import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
import os

def main():
    for filename in ['./build/1_a', './build/1_b']:
        print(filename)
        plot(filename)
    plot2a()
    plot2a1()

def plot(filename):
    r, x = np.genfromtxt(filename + '.txt', unpack=True)
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.scatter(r, x, s=0.01, alpha=0.7)
    #fig.savefig(filename + '.pdf')
    ax.set_xlabel("$r$")
    ax.set_xlabel("$x$")
    fig.savefig(filename + '.png', dpi=300)

def plot2a():
    filename = "./build/2_a"
    data = np.genfromtxt(filename + '.txt', unpack=True)
    for i in range(4):
        fig = plt.figure()
        ax = fig.add_subplot(111)
        ax.plot(data[0], data[i+1], label='$g_' + str(i) + '$')
        ax.legend(loc='best', ncol=4)
        if i == 0:
            ax.set_xlim(1.5, 2.5)
        if i == 1:
            ax.set_xlim(3, 3.3)
        if i == 2:
            ax.set_xlim(3.4, 3.57)
        if i == 3:
            ax.set_xlim(3.5, 3.57)
        #ax.set_ylim(-0.3, 0.7)
        ax.set_xlabel("$x$")
        ax.set_xlabel("$r$")
        fig.savefig(filename + str(i) +  '.pdf')

def plot2a1():
    filename = "./build/2_a"
    data = np.genfromtxt(filename + '.txt', unpack=True)
    fig = plt.figure()
    ax = fig.add_subplot(111)
    for i in range(4):
        ax.plot(data[0], data[i+1], label='$g_' + str(i) + '$')
    ax.legend(loc='best', ncol=4)
    ax.set_xlim(0, 3.57)
    ax.set_ylim(-0.3, 0.7)
    ax.set_xlabel("$x$")
    ax.set_xlabel("$r$")
    fig.savefig(filename + '.pdf')

if __name__ == '__main__':
    main()
