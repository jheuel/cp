#!/usr/bin/env python
# encoding: utf-8

import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
from scipy.stats import norm

build_dir = "./build/"

def Aufgabe_1():
    for i in range(4):
        data = np.genfromtxt(build_dir + '1_' + str(i) + '.txt', unpack=True)
        f, ax = plt.subplots()
        ax.set_xlabel("Wert")
        ax.set_ylabel("Anzahl")
        sns.set_context("paper")
        sns.set_context("notebook", font_scale=1.0, rc={"lines.linewidth": 1.5})
        sns.barplot(data[0], data[1])
        f.savefig(build_dir + '1_' + str(i) + '.pdf', dpi=300)

        data = np.genfromtxt(build_dir + '1_'+ str(i) + '_c.txt', unpack=True)
        f, ax = plt.subplots()
        ax.set_xlabel("$r_n$")
        ax.set_ylabel("$r_{n-1}$")
        sns.set_context("paper")
        sns.set_context("notebook", font_scale=1.0, rc={"lines.linewidth": 1.5})
        ax.plot(data[0], data[1], '.')
        ax.set_xlim(-0.1, 1.1)
        ax.set_ylim(-0.1, 1.1)
        f.savefig(build_dir + '1_' + str(i) + '_c.png', dpi=150)

def Aufgabe_2():
    # i
    data = np.genfromtxt(build_dir + '2_0.txt', unpack=True)
    f, ax = plt.subplots()
    ax.set_xlabel("Wert")
    ax.set_ylabel("Anzahl")
    sns.set_context("paper")
    sns.set_context("notebook", font_scale=1.0, rc={"lines.linewidth": 1.5})
    ax.hist(data, normed=True, bins = 30, alpha=.7)

    mu, std = norm.fit(data)
    xmin, xmax = plt.xlim()
    x = np.linspace(xmin, xmax, 1000)
    p = norm.pdf(x, mu, std)
    #ax.plot(x, p, 'k', linewidth=2)
    ax.plot(x, p, 'k')
    title = "Fit Ergebnisse: $\mu$ = %.2f,  $\sigma$ = %.2f" % (mu, std)
    plt.title(title)

    f.savefig(build_dir + '2_0.pdf')

    # ii
    data = np.genfromtxt(build_dir + '2_1.txt', unpack=True)
    f, ax = plt.subplots()
    ax.set_xlabel("Wert")
    ax.set_ylabel("Anzahl")
    sns.set_context("paper")
    sns.set_context("notebook", font_scale=1.0, rc={"lines.linewidth": 1.5})

    mu, std = norm.fit(data)
    #z = (data - mu) / std;
    ax.hist(data, bins = 30, normed=True, alpha=.7)
    #mu, std = norm.fit(z)
    mu, std = norm.fit(data)
    xmin, xmax = plt.xlim()
    x = np.linspace(xmin, xmax, 1000)
    p = norm.pdf(x, mu, std)
    #ax.plot(x, p, 'k', linewidth=2)
    ax.plot(x, p, 'k')
    title = "Fit Ergebnisse: $\mu$ = %.2f,  $\sigma$ = %.2f" % (mu, std)
    plt.title(title)

    f.savefig(build_dir + '2_1.pdf')

    # iii
    data = np.genfromtxt(build_dir + '2_2.txt', unpack=True)
    f, ax = plt.subplots()
    ax.set_xlabel("Wert")
    ax.set_ylabel("Anzahl")
    sns.set_context("paper")
    sns.set_context("notebook", font_scale=1.0, rc={"lines.linewidth": 1.5})
    ax.hist(data, normed=True, bins = 20, alpha=.7)

    xmin, xmax = plt.xlim()
    x = np.linspace(xmin, 3.14015, 1000)
    p = 0.5 * np.sin(x)
    ax.plot(x, p, 'k', label='$p_1(x)=\\frac{1}{2}\sin(x)$')
    #title = "Fit Ergebnisse: $\mu$ = %.2f,  $\sigma$ = %.2f" % (mu, std)
    #plt.title(title)
    plt.legend(loc='best')
    f.savefig(build_dir + '2_2.pdf')

    # iv
    data = np.genfromtxt(build_dir + '2_3.txt', unpack=True)
    f, ax = plt.subplots()
    ax.set_xlabel("Wert")
    ax.set_ylabel("Anzahl")
    sns.set_context("paper")
    sns.set_context("notebook", font_scale=1.0, rc={"lines.linewidth": 1.5})
    ax.hist(data, normed=True, bins = 20, alpha=.7)

    xmin, xmax = plt.xlim()
    x = np.linspace(xmin, xmax, 1000)
    p = 3 * x**2
    ax.plot(x, p, 'k', label='$p_2(x)=3x^2$')
    #title = "Fit Ergebnisse: $\mu$ = %.2f,  $\sigma$ = %.2f" % (mu, std)
    #plt.title(title)
    plt.legend(loc='best')
    f.savefig(build_dir + '2_3.pdf')

def main():
    Aufgabe_1()
    Aufgabe_2()


if __name__ == '__main__':
    main()
