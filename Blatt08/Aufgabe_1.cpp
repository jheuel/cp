#include <algorithm>
#include <array>
#include <cmath>
#include <fstream>

using std::array;
using std::ofstream;
using std::string;
using std::min;

int64_t r_n(int64_t r, int64_t a, int64_t c, int64_t m) {
  return (a * r + c) % m;
}

template <size_t N>
array<double, N> zufallsZahlen(int64_t r, int64_t a, int64_t c, int64_t m) {
  array<double, N> zZ;
  for (auto &i : zZ) {
    r = r_n(r, a, c, m);
    i = static_cast<double>(r) / static_cast<double>(m);
  }
  return zZ;
}

template <size_t N, size_t M>
array<size_t, M> makeHistogram(array<double, N> zZ) {
  array<size_t, M> hist;
  hist.fill(0.);
  for (auto i : zZ) {
    size_t index = static_cast<size_t>(i * 10);
    hist[index]++;
  }
  return hist;
}

template <size_t M>
void ausgabe(array<size_t, M> hist, string name) {
  ofstream output("./build/1_" + name + ".txt");
  for (auto i = 0; i < M; i++) {
    output << static_cast<double>(i) / static_cast<double>(M) << "\t" << hist[i]
           << "\n";
  }
  output.close();
}

template <size_t N>
void test2d(array<double, N> zZ, int64_t m, string name) {
  ofstream output("./build/1_" + name + "_c.txt");
  size_t n = min(static_cast<size_t>(m), N);
  for (size_t i = 1; i < n; i+=2) {
    output << zZ[i] << "\t" << zZ[i - 1] << "\n";
  }
  output.close();
}

template <size_t N, size_t M>
void test(int64_t r, int64_t a, int64_t c, int64_t m, string name) {
  auto zZ = zufallsZahlen<N>(r, a, c, m);
  auto hist = makeHistogram<N, M>(zZ);
  ausgabe<M>(hist, name);
  test2d<N>(zZ, m, name);
}

int main() {
  const size_t N = 10000;
  const size_t M = 10;

  test<N, M>(1234, 20, 120, 6075, "0");
  test<N, M>(1234, 137, 187, 256, "1");
  test<N, M>(123456789, 65539, 0, 2147483648, "2");
  test<N, M>(1234, 16807, 0, pow(2, 31) - 1, "3");

  return 0;
}
