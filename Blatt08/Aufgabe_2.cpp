#include <array>
#include <cfloat>
#include <cmath>
#include <fstream>
#include <iostream>
#include <random>

using std::cout;
using std::endl;
using std::make_pair;
using std::array;
using std::ofstream;
using std::pair;
using std::string;

pair<double, double> Box_Muller_Step(double x1, double x2) {
  double h = sqrt(-2. * log(x1));
  double y1 = h * cos(2 * M_PI * x2);
  double y2 = h * sin(2 * M_PI * x2);
  return make_pair(y1, y2);
}

template <size_t N>
array<double, N> Box_Muller_Algorithmus() {
  std::mt19937 mt(0);
  // gleichverteilt von [0,1[
  std::uniform_real_distribution<double> dist(0, 1);
  array<double, N> zZ;
  for (size_t i = 0; i < N - 1; i += 2) {
    auto x = Box_Muller_Step(dist(mt), dist(mt));
    zZ[i] = x.first;
    zZ[i + 1] = x.second;
  }
  auto x = Box_Muller_Step(dist(mt), dist(mt));
  zZ[N - 2] = x.first;
  if (N % 2 == 0) {
    zZ[N - 1] = x.second;
  }
  return zZ;
}

template <size_t N>
array<double, N> zentralerGrenzwertsatz(size_t n) {
  std::mt19937 mt(0);
  // gleichverteilt von [0,1]
  std::uniform_real_distribution<double> dist(0, std::nextafter(1, DBL_MAX));
  array<double, N> zZ;
  double mu = 0;
  double musq = 0;
  for (size_t i = 0; i < N; i++) {
    zZ[i] = 0;
    for (size_t j = 0; j < n; j++) {
      zZ[i] += dist(mt);
    }
    mu += zZ[i];
    musq += zZ[i] * zZ[i];
  }
  mu /= static_cast<double>(N);
  musq /= static_cast<double>(N);
  double sigma = sqrt(musq - mu * mu);

  for (auto &i : zZ) {
    i = (i - mu) / sigma;
  }

  return zZ;
}

template <size_t N>
array<double, N> Neumann_Rueckweisung() {
  std::mt19937 mt(0);
  // gleichverteilt von [0,1]
  std::uniform_real_distribution<double> distx(0,
                                               std::nextafter(M_PI, DBL_MAX));
  std::uniform_real_distribution<double> disty(0, std::nextafter(0.5, DBL_MAX));
  array<double, N> zZ;

  auto p = [](double x) { return 0.5 * sin(x); };
  for (auto &i : zZ) {
    double x, y;
    do {
      x = distx(mt);
      y = disty(mt);
    } while (y > p(x));
    i = x;
  }
  return zZ;
}

template <size_t N>
array<double, N> Transformationsmethode() {
  std::mt19937 mt(0);
  // gleichverteilt von [0,1]
  std::uniform_real_distribution<double> dist(0, std::nextafter(1, DBL_MAX));
  array<double, N> zZ;

  for (auto &i : zZ) {
    i = pow(dist(mt), 1. / 3.);
  }
  return zZ;
}

template <size_t N>
void ausgabe(array<double, N> zZ, string name) {
  ofstream outfile("./build/2_" + name + ".txt");
  for (auto i : zZ) {
    outfile << i << "\n";
  }
  outfile.close();
}

int main() {
  const size_t N = 1e4;

  auto zZ = Box_Muller_Algorithmus<N>();
  ausgabe<N>(zZ, "0");

  size_t nSum = 25;
  auto zZ1 = zentralerGrenzwertsatz<N>(nSum);
  ausgabe<N>(zZ1, "1");

  auto zZ2 = Neumann_Rueckweisung<N>();
  ausgabe<N>(zZ2, "2");

  auto zZ3 = Transformationsmethode<N>();
  ausgabe<N>(zZ3, "3");

  return 0;
}
